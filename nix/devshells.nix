{pkgs}: let
  getDevshells = dir: let
    entries = builtins.readDir dir;

    procEntry = name: type: let
      path = dir + "/${name}";
    in
      if type == "directory"
      then
        (
          if builtins.pathExists (path + "/devshell.nix")
          then [path]
          else []
        )
      else [];
  in
    builtins.concatLists (
      builtins.attrValues (
        builtins.mapAttrs procEntry entries
      )
    );

  buildDevshell = path: {
    name = builtins.baseNameOf (toString path);
    value = pkgs.callPackage (path + "/devshell.nix") {};
  };
in
  builtins.listToAttrs (builtins.map buildDevshell (getDevshells ../src))
